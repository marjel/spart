import { REQUEST_ACTIVITIES, RECEIVE_ACTIVITIES } from '../constants/ActionTypes';

export function receiveActivities(response) {
  return {
    type: RECEIVE_ACTIVITIES,
    response
  }
}

export function requestActivities(request) {
  return {
    type: REQUEST_ACTIVITIES,
    request
  }
}
