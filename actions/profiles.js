import { ADD_PROFILE, REMOVE_PROFILE, SET_PROFILE_IMPORTANT, UNSET_PROFILE_IMPORTANT } from '../constants/ActionTypes';

export function addProfile(profileId) {
  return {
    type: ADD_PROFILE,
    profileId
  }
}

export function removeProfile(profileId) {
  return {
    type: REMOVE_PROFILE,
    profileId
  }
}


export function setProfileImportant(profileId) {
  return {
    type: SET_PROFILE_IMPORTANT,
    profileId
  }
}

export function unsetProfileImportant(profileId) {
  return {
    type: UNSET_PROFILE_IMPORTANT,
    profileId
  }
}
