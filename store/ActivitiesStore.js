import { createStore } from 'redux';
import { activities } from '../reducers/ActivityReducer';

export default function ActivitiesStore(initialState) {
  const store = createStore(activities, initialState);

  if (module.hot) {
    module.hot.accept('../reducers', () =>
      store.replaceReducer(require('../reducers').default)
    );
  }
  return store;
}
