import { createStore } from 'redux';
import applicationReducer from '../reducers';

export default function configureStore(initialState) {
  const store = createStore(applicationReducer, initialState);

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default;
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}
