import { createStore } from 'redux';
import { profiles } from '../reducers/ProfilesReducer';

export default function ProfilesStore(initialState) {
    const store = createStore(profiles, initialState);

    if (module.hot) {
        module.hot.accept('../reducers', () =>
            store.replaceReducer(require('../reducers').default)
        );
    }
    return store;
}
