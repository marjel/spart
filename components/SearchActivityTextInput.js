import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import { REQUEST_ACTIVITIES, RECEIVE_ACTIVITIES } from '../constants/ActionTypes';

class SearchActivityTextInput extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      text: this.props.text || ''
    };
  }

  handleSubmit(e) {
    const text = e.target.value.trim();
    if (e.which === 13) {
      this.props.onChange(text);
    }
  }

  handleChange(e) {
    this.setState({ text: e.target.value });
  }

  handleBlur(e) {
    if (!this.props.newTodo) {
      this.props.onChange(e.target.value);
    }
  }

  render() {
    return (
        <input className={
        classnames({
          edit: this.props.editing,
          'new-activity': this.props.newActivity
        })}
               type="text"
               placeholder={this.props.placeholder}
               autoFocus="true"
               value={this.state.text}
               onBlur={this.handleBlur.bind(this)}
               onChange={this.handleChange.bind(this)}
               onKeyDown={this.handleSubmit.bind(this)} />
    )
  }
}

SearchActivityTextInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  text: PropTypes.string,
  placeholder: PropTypes.string,
  editing: PropTypes.bool,
  newActivity: PropTypes.bool
};


export default SearchActivityTextInput;
