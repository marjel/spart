import React, { Component, PropTypes } from 'react'
import classnames from 'classnames'

class ActivityItem extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
        editing: false
        };
    }

    handleSave(id, text) {
        if (text.length === 0) {
            this.deleteItem(id);
         }
        this.setState();
    }

    deleteItem(id) {
        if (text.length === 0) {

        }
        this.setState();
    }

    render() {
        const { activity } = this.props;
        let element;
        element = (
            <div className="view">
                <p>{activity.text}</p>
                <input type="checkbox"/>
                <button className="destroy" onClick={() => this.deleteItem(activity.id)} />
            </div>
        );
        return (
          <li>
            {element}
          </li>
        );
  }
}

ActivityItem.propTypes = {
  activity: PropTypes.object.isRequired
};

export default ActivityItem;
