import React, { PropTypes, Component } from 'react';
import SearchActivityTextInput from './SearchActivityTextInput';
import { GetActivities } from '.././services/GooglePlusService';

class Header extends Component {

  handleChangeInput(text) {
    if (text.length !== 0) {
      GetActivities(text);
    }
  }

  render() {
    return (
      <header className="header">
          <h1><i>G+ Activity</i> v.0.1</h1>
          <SearchActivityTextInput newActivity
                         onChange={this.handleChangeInput.bind(this)}
                         placeholder="Search Google+ Profiles Activity..." />
      </header>
    )
  }
}

export default Header;
