import React, { Component, PropTypes } from 'react';
import ActivityItem from './ActivityItem';
import Footer from './Footer';

class MainSection extends Component {
  constructor(props, context) {
    super(props, context);
  }

  renderToggleAll(completedCount) {
    const { activities, actions } = this.props;
    if (activities.length > 0) {
      return (
        <input className="toggle-all" type="checkbox" />
      )
    }
  }

  renderFooter(completedCount) {
    const { activities } = this.props;
    const activeCount = activities.length - completedCount;

    if (activities.length) {
      return (
        <Footer activeCount={activeCount} />
      );
    }
  }

  render() {
    const { activities, actions } = this.props;

    const completedCount = activities.reduce((count, todo) =>
      todo.completed ? count + 1 : count,
      0
    )

    return (
      <section className="main">
        {this.renderToggleAll(completedCount)}
        <ul className="activity-list">
          {activities.map(todo =>
            <ActivityItem key={todo.id} activity={todo} {...actions} />
          )}
        </ul>
        {this.renderFooter(completedCount)}
      </section>
    )
  }
}

MainSection.propTypes = {
  activities: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
}

export default MainSection;
