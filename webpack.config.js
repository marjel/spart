var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: [
    'webpack-hot-middleware/client',
    './index'
  ],
  node: {
    console: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: [ 'babel?compact=false' ]
      },
      {
        test: /\.css?$/,
        loaders: [ 'style', 'css' ],
        exclude: /node_modules/,
        include: __dirname
      }
    ]
  },
  resolve: {
    modulesDirectories: ['node_modules']
  }
};

var __nodeModules = path.join(__dirname, 'node_modules');
var fs = require('fs');
if (fs.existsSync(__nodeModules)) {
  process.env.BABEL_ENV = 'commonjs';

  module.exports.module.loaders.push({
        test: /\.json$/,
        loaders: [ 'json' ],
        include: __nodeModules
      }/*,{
    test: /\.js$/,
    loaders: [
      'babel?compact=false',
      'transform?0'
    ],
    include: __nodeModules
  }*/);
}