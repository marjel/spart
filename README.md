Test Task. React js; Redux js; webpack;
Google+ API [browser API Key]

Prepared with:

* Ubuntu 14.4
* JetBrains WebStorm [current EAP] - You can use settings.jar to configure your local IDE
* Newest npm; webpack & other js dependencies

Run:

1. npm install [installing dependencies defined in package.json]
2. npm start ['cleans' & 'compiles' the application then runs its in the default web browser]
