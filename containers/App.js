import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from '../components/Header';
import MainSection from '../components/MainSection';
import * as activities from '../actions/activities';
import * as profiles from '../actions/profiles';

class App extends Component {
  render() {
    const { activities, actions } = this.props;
    return (
      <div>
        <Header />
        <MainSection activities={activities} actions={actions} />
      </div>
    );
  }
}

App.propTypes = {
  activities: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
}

function mapStateToProps(state) {
  return {
    activities: state.activities
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(activities, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
