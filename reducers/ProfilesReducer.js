import { ADD_PROFILE, REMOVE_PROFILE, SET_PROFILE_IMPORTANT, UNSET_PROFILE_IMPORTANT } from '../constants/ActionTypes';

const initialState = [{
  text: 'Profiles',
  marked: false,
  id: 0
}];

export function profiles(state = initialState, action = ADD_PROFILE) {
  switch (action.type) {
    case ADD_PROFILE:
      return [{
        id: (state.length === 0) ? 0 : state[0].id + 1,
        marked: false,
        text: action.text
      }, ...state];
    case REMOVE_PROFILE:
      return [{
        id: (state.length === 0) ? 0 : state[0].id - 1,
        marked: false,
        text: action.text
      }, ...state];
    case SET_PROFILE_IMPORTANT:
      return [{
        id: (state.length === 0) ? 0 : state[0].id - 1,
        marked: false,
        text: action.text
      }, ...state];

    default:
      return state;
  }
}
