import { combineReducers } from 'redux';
import { activities } from './ActivityReducer';
import { profiles } from './ProfilesReducer';

const applicationReducer = combineReducers({
  activities, profiles
})

export default applicationReducer;
