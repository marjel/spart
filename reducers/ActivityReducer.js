import { REQUEST_ACTIVITIES, RECEIVE_ACTIVITIES } from '../constants/ActionTypes';

const initialState = [{
  text: 'Activities',
  id: 0
}];

export function activities(state = initialState, action) {
  switch (action.type) {
    case REQUEST_ACTIVITIES:
      return [{
        id: (state.length === 0) ? 0 : state[0].id + 1,
        marked: false,
        text: action.text
      }, ...state];
    case RECEIVE_ACTIVITIES:
      return [{
        id: (state.length === 0) ? 0 : state[0].id + 1,
        marked: false,
        text: action.text
      }, ...state];

    default:
      return state;
  }
}
